# Numerical Modelling of Atmosphere and Oceans_Project 1
For the results in the form of a Python notebook:

Run MTMW14_Project1.ipynb

For the results and plots of QA to QF:

Run climate_model_A_F.py

For the results and plots of QG:

Run climate_model_G.py
