"""
run_climate_model.py

A simple climate model.
"""

import numpy as np
import random
import math
import matplotlib.pyplot as plt
from __init__ import (b0, gama, c, r, alpha, dt, T0, h0, fann, fran, taucor)
from sol import RK2

def main():
    nt  = 41 # iteration month, for simplification 1 month = 30 days
    for num in range(20):
        print(num)
        T, h = [T0+random.uniform(-0.5/7.5,0.5/7.5)], [h0+random.uniform(-5/150,5/150)]
        for i in range(int((nt-1)/dt)):
           it = i*dt
           miu = 0.75*(1+ 0.2*math.cos(2*math.pi*it/(12/2)-5*math.pi/6)) # for task D
           #miu = 2/3 ### for task A B C 
           en  = 0.1
           b = b0*miu
           R = gama*b-c
           ksi = fann*math.cos(2*math.pi*it/(12/2))+fran*random.uniform(-1, 1)*taucor/dt
           Th_new = RK2(R, gama, r, alpha, b, en, ksi, T[i], h[i], dt)
           T.append(Th_new[0])
           h.append(Th_new[1])
        
        T = np.multiply(T,7.5)
        h = np.multiply(h,150)
        fig1 = plt.figure(1)
        plt.plot(np.linspace(1,nt,int((nt-1)/dt)+1)*2,T)
        plt.xlabel('t (month)')
        plt.ylabel('T_E (K)')
        fig1 = plt.figure(2)
        plt.plot(np.linspace(1,nt,int((nt-1)/dt)+1)*2,h)
        plt.xlabel('t (month)')
        plt.ylabel('h_W (m)')
        
        fig3 = plt.figure(3)
        plt.plot(T,h)
        plt.xlabel('T_E (K)')
        plt.ylabel('h_W (m)')
        
    return T, h
    
if __name__ == '__main__':
    T, h = main()
