"""
run_climate_model.py

A simple climate model.
"""

import numpy as np
import random
import math
import matplotlib.pyplot as plt
from __init__ import (b0, gama, c, r, alpha, dt, T0, h0, fann, fran, taucor)
from sol import RK2

def main():
    nt  = 22*5 # iteration months
    T, h = [T0], [h0]
    for i in range(int((nt-1)/dt)):
        it = i*dt
        miu = 0.75*(1+ 0.2*math.cos(2*math.pi*it/(12/2)-5*math.pi/6)) # for task D
        #miu = 2/3 ### for task A B C E
        en  = 0.1
        b = b0*miu
        R = gama*b-c
        ksi = fann*math.cos(2*math.pi*it/(12/2))+fran*random.uniform(-1, 1)*taucor/dt
        Th_new = RK2(R, gama, r, alpha, b, en, ksi, T[i], h[i], dt)
        T.append(Th_new[0])
        h.append(Th_new[1])
    
    T = np.multiply(T,7.5)
    h = np.multiply(h,150)
    fig1 = plt.figure(1)
    plt.plot(np.linspace(1,nt,int((nt-1)/dt)+1)*2,T,'b',label = 'T_E')
    plt.xlabel('t (month)')
    plt.ylabel('T_E (K)')
    plt.show()
    
    fig2 = plt.figure(2)
    plt.plot(np.linspace(1,nt,int((nt-1)/dt)+1)*2,h,'r--',label= 'h_W')
    plt.xlabel('t (month)')
    plt.ylabel('h_W (m)')
    plt.show()
    
    fig3 = plt.figure(3)
    plt.plot(T,h)
    plt.xlabel('T_E (K)')
    plt.ylabel('h_W (m)')
    plt.show()
    
    return T, h
    
if __name__ == '__main__':
    T, h = main()
