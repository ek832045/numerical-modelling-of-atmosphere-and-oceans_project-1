"""
RK2
"""
import numpy as np

def F1(R, gama, r, alpha, b, en, ksi, T, h):
    M = np.array([[R, gama],
                 [-alpha*b, -r]])
    var = np.array([T, h])
    v   = np.array([-en*(h+b*T)**3+gama*ksi, -alpha*ksi])
    kl  = np.dot(M,var)+v
    return kl

def RK2(R, gama, r, alpha, b, en, ksi, T, h, dt):
    """
    :param t: The initial value of t
    :param x: The initial value of x
    :param y: The initial value of y
    :param h: Time step
    :return: Iteration new solution
    """
    
    kl_1 = F1(R, gama, r, alpha, b, en, ksi, T, h)
    kl_2 = F1(R, gama, r, alpha, b, en, ksi, T + dt*kl_1[0], h + dt*kl_1[1])  
    
    Th_new = [T, h] + (kl_1 + kl_2 ) * dt / 2
    return Th_new

    
    
