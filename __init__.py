"""

"""
# Constants:

b0     = 2.5
gama   = 0.75
c      = 1
r      = 0.25
alpha   = 0.125
#
fann  = 0.02
fran  = 0.2
taucor   = 1/30/2 # day->month
tau   = 12/2 # month

# Time step, months
dt = 1/30/2. # month, for task E F, for simplification 1 month = 30 days
# T & h initialization
T0     = 1.125/7.5
h0     = 0/150

